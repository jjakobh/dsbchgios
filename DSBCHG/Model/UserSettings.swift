//
//  UserSettings.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 23.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import Foundation

import Combine
import GRPC

let classes = ["ALL", "5", "6", "7", "8", "9", "EF", "Q1", "Q2"]

class UserSettings: ObservableObject {
    let objectWillChange = ObservableObjectPublisher()

    static var server = ("jjakobh.ddns.net", 1337)
    
    @UserDefault("isLoggedIn", defaultValue: false)
    private(set) var isLoggedIn
    @UserDefault("username", defaultValue: "")
    private(set) var username: String
    @UserDefault("password", defaultValue: "")
    private(set) var password: String
    
    @UserDefault("enableNotifications", defaultValue: true)
    var enableNotifications: Bool { willSet { objectWillChange.send() }}
    @UserDefault("simplifiedNotifications", defaultValue: true)
    var simplifiedNotifications: Bool { willSet { objectWillChange.send() }}
    
    @UserDefault("classFilter", defaultValue: "ALL")
    var classFilter: String { willSet { objectWillChange.send() }}
    
    var auth: Dsbapi_Auth { return Dsbapi_Auth.with { $0.username = username; $0.password = password } }

    func client() -> Dsbapi_DSBApiServiceClient {
        let configuration = ClientConnection.Configuration(
            target: ConnectionTarget.hostAndPort(UserSettings.server.0, UserSettings.server.1),
            eventLoopGroup: PlatformSupport.makeEventLoopGroup(loopCount: 1)
        )
        let client = Dsbapi_DSBApiServiceClient(connection: ClientConnection(configuration: configuration))
        client.defaultCallOptions.timeout = try! .seconds(4) /* amount not negative and not more than 8 digits long. */
        return client
    }
    
    
    func login(username: String, password: String, simplifiedNotifications: Bool) {
        objectWillChange.send()
        self.isLoggedIn = true
        self.username = username
        self.password = password
        self.simplifiedNotifications = simplifiedNotifications
    }
    
    func logout() {
        objectWillChange.send()
        self.isLoggedIn = false
    }
}
