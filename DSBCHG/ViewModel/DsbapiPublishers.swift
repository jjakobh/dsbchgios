//
//  DsbapiPublishers.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 25.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import Foundation
import Combine
import GRPC
import CombineGRPC

class SubstitutionTablesStore: ObservableObject {
    @Published var list: [Dsbapi_SubstitutionTable] = []
    @Published var error: GRPCStatus?

    private var searchCancellable: Cancellable? { didSet { oldValue?.cancel() } }
    deinit { searchCancellable?.cancel() }
    
    func search(client: Dsbapi_DSBApiServiceClient, auth: Dsbapi_Auth) {
        searchCancellable = CombineGRPC.call(client.getSubstitutionTables)(auth)
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { completion in
                    if case let .failure(status) = completion {
                        self.error = status
                    }
                },
                receiveValue: { self.list.append($0) }
            )
    }
}

class TilesStore: ObservableObject {
    @Published var list: [Dsbapi_Tile] = []
    @Published var error: GRPCStatus?

    private var searchCancellable: Cancellable? { didSet { oldValue?.cancel() } }
    deinit { searchCancellable?.cancel() }
    
    func search(client: Dsbapi_DSBApiServiceClient, auth: Dsbapi_Auth) {
        searchCancellable = CombineGRPC.call(client.getTiles)(auth)
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { completion in
                    if case let .failure(status) = completion {
                        self.error = status
                    }
                },
                receiveValue: { self.list.append($0) }
            )
    }
}

class NewsStore: ObservableObject {
    @Published var list: [Dsbapi_News] = []
    @Published var error: GRPCStatus?

    private var searchCancellable: Cancellable? { didSet { oldValue?.cancel() } }
    deinit { searchCancellable?.cancel() }
    
    func search(client: Dsbapi_DSBApiServiceClient, auth: Dsbapi_Auth) {
        searchCancellable = CombineGRPC.call(client.getNews)(auth)
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { completion in
                    if case let .failure(status) = completion {
                        self.error = status
                    }
                },
                receiveValue: { self.list.append($0) }
            )
    }
}

class EventsStore: ObservableObject {
    @Published var list: [Dsbapi_Event] = []
    @Published var error: GRPCStatus?

    private var searchCancellable: Cancellable? { didSet { oldValue?.cancel() } }
    deinit { searchCancellable?.cancel() }
    
    func search(client: Dsbapi_DSBApiServiceClient) {
        searchCancellable = CombineGRPC.call(client.getEvents)(Dsbapi_Empty())
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { completion in
                    if case let .failure(status) = completion {
                        self.error = status
                    }
                },
                receiveValue: { self.list.append($0) }
            )
    }
}

class TimetablesStore: ObservableObject {
    @Published var list: [String] = []
    @Published var error: GRPCStatus?

    private var searchCancellable: Cancellable? { didSet { oldValue?.cancel() } }
    deinit { searchCancellable?.cancel() }
    
    func search(client: Dsbapi_DSBApiServiceClient, auth: Dsbapi_Auth) {
        searchCancellable = CombineGRPC.call(client.getTimetables)(auth)
            .map(\.urls)
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { completion in
                    if case let .failure(status) = completion {
                        self.error = status
                    }
                },
                receiveValue: { self.list = $0 }
            )
    }
}
