//
//  MainScreen.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 24.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import SwiftUI

import Combine
import CombineGRPC
import GRPC

struct MainScreen: View {
    @State var selectedView: Int = 0
    
    let tabs = [
        ("Substitutions", "house"),
        ("Tiles", "rectangle.3.offgrid.fill"),
        ("Events", "calendar"),
    ]
    
    func tabItem(_ desc: (String, String)) -> some View {
        return VStack {
            Image(systemName: desc.1)
            Text(desc.0)
        }
    }
    
    var body: some View {
        NavigationView {
            TabView(selection: $selectedView) {
                SubstitutionTablesView()
                    .tabItem { tabItem(tabs[0]) }.tag(0)
                TilesView()
                    .tabItem { tabItem(tabs[1]) }.tag(1)
                EventsView()
                    .tabItem { tabItem(tabs[2]) }.tag(2)
            }
                .navigationBarTitle(tabs[selectedView].0)
                .navigationBarItems(trailing: NavigationLink(destination: SettingsView()) {
                    Image(systemName: "person.crop.circle").imageScale(.large).padding()
                })
        }
    }
}


struct MainScreen_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen()
    }
}
