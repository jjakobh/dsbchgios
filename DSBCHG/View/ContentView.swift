//
//  ContentView.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 23.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        VStack {
            if !settings.isLoggedIn {
                LoginView()
            } else {
                MainScreen()
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
