//
//  EventsView.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 23.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import SwiftUI

struct EventsView: View {
    @EnvironmentObject var settings: UserSettings
    @ObservedObject var events = EventsStore()
    
    var body: some View {
        List {
            ForEach(events.list, id: \.self) { event in
                EventView(event: event)
            }
        }.onAppear(perform: reload)
    }
    
    func reload() {
        self.events.search(client: settings.client())
    }
}

struct EventView: View {
    let event: Dsbapi_Event
    
    var body: some View {
        Text(event.title)
    }
}

struct EventsView_Previews: PreviewProvider {
    static var previews: some View {
        EventsView()
    }
}
