//
//  SettingsView.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 23.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import SwiftUI


struct SettingsView: View {
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        Form {
            Section(header: Text("Login data")) {
                Text("Username: \(settings.username)")
                Text("Password: \(settings.password)")
            }
            Section(header: Text("Notifications")) {
                Toggle(isOn: $settings.enableNotifications) {
                    Text("Enable notifications")
                }
                Toggle(isOn: $settings.simplifiedNotifications) {
                    VStack(alignment: .leading) {
                        Text("Simplified notifications")
                        Text("Show notifications summarized by count and class").font(.caption)
                    }
                }.disabled(!settings.enableNotifications)
            }
            Section(header: Text("Filters")) {
                Picker(selection: $settings.classFilter, label: Text("Class")) {
                    ForEach(classes, id: \.self) { Text($0) }
                }.pickerStyle(SegmentedPickerStyle())
            }
            
            Button(action: settings.logout) {
                Text("Log out")
            }
        }
            .navigationBarTitle("Settings")
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
