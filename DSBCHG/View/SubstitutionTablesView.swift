//
//  SubstitutionsView.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 23.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import SwiftUI

struct SubstitutionTablesView: View {
    @EnvironmentObject var settings: UserSettings
    @ObservedObject var substitutionTables = SubstitutionTablesStore()
    
    var body: some View {
        List {
            ForEach(substitutionTables.list, id: \.date) { table in
                SubstitutionTableView(table: table)
            }
        }.onAppear(perform: reload)
    }
    
    func reload() {
        self.substitutionTables.search(client: settings.client(), auth: settings.auth)
    }
}


struct SubstitutionTableView: View {
    let table: Dsbapi_SubstitutionTable
    
    var body: some View {
        Section(header: Text(dateFormat(table.date))) {
            if table.substitutions.count == 0 {
                Text("No Substitutions :(")
            } else {
                ForEach(table.substitutions.filter { $0.classes.contains("Q1") }, id: \.self) { sub in
                    SubstitutionView(substitution: sub)
                }
            }
        }
    }
}

struct SubstitutionView: View {
    let substitution: Dsbapi_Substitution
    
    var body: some View {
        Text(String(describing: substitution))
    }
}

func dateFormat(_ date: Dsbapi_Date) -> String {
    return "\(date.day).\(date.month).\(date.year)"
}


struct SubstitutionTableView_Previews: PreviewProvider {
    static var previews: some View {
        SubstitutionTableView(table: Dsbapi_SubstitutionTable.with {
            $0.date = Dsbapi_Date.with { $0.year = 2020; $0.month = 5; $0.day = 23 }
            $0.affected = ["7A", "7B", "EF", "Q1", "SE"]
            $0.absent = ["PS", "ZG", "KÜ", "LM"]
        })
    }
}

struct SubstitutionTablesView_Previews: PreviewProvider {
    static var previews: some View {
        SubstitutionTablesView()
    }
}

struct SubstitutionView_Previews: PreviewProvider {
    static var previews: some View {
        SubstitutionView(substitution: Dsbapi_Substitution.with {
            $0.pretty = "[Q1]: 3-4. Stunde Info Enfall bei ZG"
        })
    }
}


