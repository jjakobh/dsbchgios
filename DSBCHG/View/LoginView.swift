//
//  LoginView.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 23.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import SwiftUI
import GRPC
import CombineGRPC
import Combine

enum LoginStatus {
    case success
    case failed
    case networkError(String?)
    case loading
    
    func text() -> String {
        switch self {
        case .success:
            return "Login succeeded!"
        case .failed:
            return "Login failed!"
        case .networkError(let msg):
            return "Network error: \(msg ?? "server unreachable")"
        case .loading:
            return "loading..."
        }
    }
    
    func color() -> Color {
        switch self {
        case .success:
            return Color.green
        case .failed:
            return Color.red
        case .networkError:
            return Color.red
        case .loading:
            return Color.gray
        }
    }
}

struct LoginView: View {
    @EnvironmentObject var settings: UserSettings
    
    @State var username = ""
    @State var password = ""
    @State var simplifiedNotifications = true
    
    @State var isEditing = false
    
    @State var loginStatus: LoginStatus? = nil
    
    var body: some View {
        ZStack {
            VStack {
                header()
                
                usernameTextField()
                passwordSecureField()
                toggles()
                loginButton()
            }.padding()
            
            if loginStatus != nil {
                loginStatusMessage(for: loginStatus!)
            }
        }.offset(y: isEditing ? -150 : 0)
    }
    
    func header() -> some View {
        VStack {
            Image("DSBCHG")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 200, height: 200)
                .clipped()
                .padding(.bottom, 75)
                
        }
    }
  
    func usernameTextField() -> some View {
        return TextField("Username", text: $username, onEditingChanged: { self.isEditing = $0 })
            .textContentType(.username)
            .padding()
            .background(Color("inputs"))
            .cornerRadius(5.0)
            .padding(.bottom, 20)
    }
    func passwordSecureField() -> some View {
        return SecureField("Password", text: $password)
            .textContentType(.password)
            .padding()
            .background(Color("inputs"))
            .cornerRadius(5.0)
            .padding(.bottom, 20)
    }
    func toggles() -> some View {
        return Toggle(isOn: $simplifiedNotifications) {
            VStack(alignment: .leading) {
                Text("Simplified notifications").fontWeight(.semibold)
                Text("Show notifications summarized by count and class").font(.caption)
            }
        }
            .padding()
            .background(Color("inputs"))
            .cornerRadius(5.0)
            .padding(.bottom, 20)
        
    }
    func loginButton() -> some View {
        return Button(action: login) {
            Text("LOGIN")
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .frame(width: 220, height: 60)
                .background(Color("accent"))
                .cornerRadius(15.0)
        }
    }
    func loginStatusMessage(for loginStatus: LoginStatus) -> some View {
        return Text(loginStatus.text())
            .font(.headline)
            .frame(width: 250, height: 80)
            .background(loginStatus.color())
            .cornerRadius(20.0)
            .foregroundColor(.white)
            .onTapGesture {
                self.loginStatus = nil
            }
    }
    
    
    func login() {
        self.loginStatus = .loading
        
        let auth = Dsbapi_Auth.with { $0.username = username; $0.password = password }
        let _ = CombineGRPC.call(settings.client().getLoginInfo)(auth)
            .sink(
                receiveCompletion: { status in
                    if case let .failure(error) = status {
                        print("FJTECH", status)
                        self.loginStatus = .networkError(error.message)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        self.loginStatus = nil
                    }
                },
                receiveValue: { response in
                    if !response.isValid {
                        self.loginStatus = .failed
                    } else {
                        self.loginStatus = .success
                        DispatchQueue.main.async {
                            self.settings.login(username: self.username, password: self.password, simplifiedNotifications: self.simplifiedNotifications)
                        }
                    }
                }
            )
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
