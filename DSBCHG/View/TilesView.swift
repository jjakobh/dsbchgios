//
//  TilesView.swift
//  DSBCHG
//
//  Created by Jakob Hellermann on 23.08.19.
//  Copyright © 2019 FJ Tech. All rights reserved.
//

import SwiftUI

struct TilesView: View {
    @EnvironmentObject var settings: UserSettings
    @ObservedObject var tiles = TilesStore()
    
    var body: some View {
        List {
            ForEach(tiles.list, id: \.id) { tile in
                TileView(tile: tile)
            }
        }.onAppear(perform: reload)
    }
    
    func reload() {
        self.tiles.search(client: settings.client(), auth: settings.auth)
    }
}

struct TilesView_Previews: PreviewProvider {
    static var previews: some View {
        TilesView()
    }
}

struct TileView: View {
    let tile: Dsbapi_Tile
    
    var body: some View {
        Text(tile.title)
    }
}

struct TileView_Previews: PreviewProvider {
    static var previews: some View {
        TileView(tile: Dsbapi_Tile.with { _ in
            
        })
    }
}
